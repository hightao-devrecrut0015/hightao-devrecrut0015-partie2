<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="CSS/style.css">
</head>

<body class="large">
    <?php
            include "../apps/controlleur.php";
            $app = new ElectionManager();

        ?>

    <section class='content-large'>
        <div class='header'>
            <a href="http://localhost/www.mahazoarivo.mg/home/candidates/" class="btn-retour">
                << Revenir à la liste des candidats</a>
        </div>
        <div class='menu'>
            <?php
                // verifier si l'id est bien définie
                if(isset($_GET['id'])){
                    // appel du fonction detailCandidat()
                    $detail = $app->detailCandidat($_GET['id']);
                    // Compter le nombre d'enregistrement pour le lien pagination.
                    $nb = sizeof((array)$app->ListCandidat());
                    
                    $id = $_GET['id'];
                    echo "<h3>Candidat n°".$id."</h3>";
                    echo "<p><b>Nom :</b> ".$detail['nom']."</p>";
                    echo "<p><b>Prenom :</b> ".$detail['prenom']."</p>";
                    echo "<p><img src='".$detail['images']."' style='width: 220px; height: 220px; border: 2px inset black' title='ANTOKO : ".$detail['antoko']."' alt='".$detail['antoko']."'/></p>";
                    echo "<table style='width:100%'><tr><td style='width:50%'>";
                    // N'affiche pas le lien de pagination si le numéro d'ordre de candidat atteint l'ordre n°1  
                    if($_GET['id']-1>0){
                        $id=$_GET['id']-1;
                        echo "<a href='candidat.php?id=".$id."' class='btn btn-success'><< candidat n°".$id."</a>";
                    }
                    echo "</td><td style='width:50%'>";
                    // N'affiche pas le lien de pagination si le numéro d'ordre de candidat atteint l'ordre maximale
                    if($_GET['id']+1<=$nb){
                        $id=$_GET['id']+1;
                        echo "<a href='candidat.php?id=".$id."' class='btn btn-success'> candidat n°".$id." >></a>";
                    }
                    echo "</td></tr></table>";
                }else{
                    echo "<p>Indefined</p>";
                }
            ?>
        </div>
    </section>

</body>
<script src="JS/jquery.min.js"></script>
<script src="JS/windowWidth.js"></script>
</html>