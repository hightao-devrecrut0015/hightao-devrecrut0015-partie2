<html>
    <head>
        <meta charset=utf-8>
        <link rel="stylesheet" href="CSS/style.css">
    </head>
    <body class="large">
        <?php
            include "../apps/controlleur.php";
            $app = new ElectionManager();
            $electeur = $app->detailElecteur();
            // recuper le nom et prenom du dernier électeur enregistré sur la liste.
            $nom = $electeur[sizeof($electeur)-1]->firstName;
            $prenom= $electeur[sizeof($electeur)-1]->lastName;
        ?>

        <section class='content-large'>
            <div class='header'>
                <h2>Merci !</h2>
            </div>
            <div class='menu'>
                <p style="color: green"><?php echo $nom." ".$prenom?></p>
                <p>Vous êtes inscrits sur la liste électorale</p>
            </div>
            <div class='header'>
            <a href="http://localhost/www.mahazoarivo.mg/" class="btn-retour">
                << Revenir à l'accueil</a>
        </div>
        </section>
    </body>
    <script src="JS/jquery.min.js"></script>
    <script src="JS/windowWidth.js"></script>
</html>
