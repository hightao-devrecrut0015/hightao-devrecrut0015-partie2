<?php

use DataBase\DBFileManager;

include "DataBase/DBFileManager.php";

    class ElectionManager{
        
       // recupérer la liste des candidats
        public function ListCandidat(){

           $file = "../data/candidates.json";
           // créer une nouvelle instance de la classa DBFileManager en passant comme valeur le fichier candidates.json
           $db = new DBFileManager($file);
           $list = $db->getData();
           return $list;
        }

        // recuperer un candidat selon son ordre dans la liste.
        public function detailCandidat($i){
            // recuperer toute la liste que nous avons déjà la haut.
            $list = $this->ListCandidat();
            $a = 0;
            //parcourir la liste
            foreach($list as $ls){
                $a++;
                // tester si le numéro d'ordre est égal à l'ordre que l'élécteur à selectionné
                if($i==$a){
                    // Si c'est la bonne numéro d'ordre, on va copié les infos sur le candidat dans une nouvelle Array.
                    $candidat = array("nom" => $ls->nom,
                                        "prenom" => $ls->prenom,
                                            "images" => $ls->images,
                                                "antoko" => $ls->antoko);
                }
            }
            return $candidat;
        }

        // Ajouter nouveau électeur 
        public function saveElecteur($a,$b,$c,$d){
                // Transformer les info sur l'electeur sous forme d'un Array. 
                $data = array("firstName" => $a,
                                "lastName"=> $b,
                                    "birthDay"=> $c,
                                        "address"=> $d);
                // Preciser le fichier pur enregistrement
                $file = "../data/voters.json";
                $db = new DBFileManager($file);
                // Rédiriger vers la page de remerciement si l'électeur est belle est bien enregistrer!
                return $db->saveData($data);
                    //header("Location:../public/message.php",true);
               // } 
        }

        //recuperer la liste des électeur enregistré
        public function detailElecteur(){
            $file = "../data/voters.json";
            $db = new DBFileManager($file);
            $list = $db->getData();
            /*foreach($list as $ls){
                    $electeur = array("firstName" => $ls->firstName,
                                        "lastName" => $ls->lastName,
                                            "birthDay" => $ls->birthDay,
                                                "address" => $ls->address);
                }*/
            return $list;
        }
    }
?>