<?php

namespace DataBase;


class DBFileManager {

    private $file;
    private $dataArray;

    // constructeur de la classe DBFileManager en initialisant le fichier Json qu'on va utiliser à chaque instance de la classe DBFileManager
    public function __construct($file) {
        $this->file = $file;
    }
    
    // transformer la contenue de notre fichier json en Array
    public function getData() {
        $dataRaw = file_get_contents($this->file);
        $this->dataArray = json_decode ($dataRaw);
        return $this->dataArray;
    }
    
    // Ajouter un nouvelle enregistrement dans notre fichier json
    public function saveData($dataArray) {
        $currentData = file_get_contents($this->file);
        // decode le fichier Json 
        $array_data = json_decode($currentData, true);
        // Ajouter le nouveau enregistrement à la fin de la liste
        $array_data[] = $dataArray;
        // encode notre Array de la liste complet en Json avec mise en forme d'affichage du fichier Json. 
        $dataRaw = json_encode ($array_data, JSON_PRETTY_PRINT, JSON_FORCE_OBJECT );
        // Tester si l'écriture de la nouvelle liste dans le fichier Json à reussi.
        if(file_put_contents($this->file, $dataRaw)){
            return true;
        }else{
            return false;
        }
    }
}
